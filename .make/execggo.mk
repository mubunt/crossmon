#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	C executable generation and processing for C and C/Java projects
#
# INPUTS
# ------
# 	ENVIRONMENT VARIABLES:
#		BIN_DIR			Pathname of released executables
#		LIB_DIR			Pathname of released libraries
#		INC_DIR			Pathname of released header files
#
# 	MAKE VARIABLES:
#		MUTE			Prevents or not the command line from echoing out to the console
#		PROGRAM 		Name of the executable to generate
#		VERSION 		Version of the executable to generate
#		SOURCES 		List of C source files
#		OBJDIR 			Relative pathname of the directory where object and executable files will be generated
#		GENGETOPTFLAFS	List of specific gengetopt options other than those defined by default
#		OPTIM			Optimization options form compilation and link
#		INCLUDES 		List of directories for header files
#		LIBS 			Additional libraries to link (ld format)
#		STRIP 			Strip utility
#		COPYRIGHT 		Year(s) for copyright clause
#
# NOTES
# -----
#	Independent 'install' and 'cleaninstall' targets may exist in other parts of parent makefile.
#------------------------------------------------------------------------------
DEPDIR		= $(OBJDIR)/deps
CMDLINE		= $(PROGRAM)_cmdline
SOURCES		+= $(CMDLINE).c
OBJECTS		= $(addprefix $(OBJDIR)/,$(SOURCES:.c=.o))
EXEC		= $(OBJDIR)/$(PROGRAM)

CFLAGS		+= -DLINUX -D_REENTRANT -DVERSION=$(VERSION)
INCLUDES	+= -I. -I.. -I$(OBJDIR) -I$(INC_DIR)
#-------------------------------------------------------------------------------
CC			= gcc
MKDIR		= mkdir -p
RM			= rm -f
RMDIR		= rm -fr
GENGETOPT	= gengetopt
INSTALL		= install -p -v -D
ASTYLE		= astyle
CPPCHECK 	= cppcheck
#-------------------------------------------------------------------------------
CCFLAGS		= $(CFLAGS) -m64 $(OPTIM) $(INCLUDES) -Wall -W -Wextra -Wno-unused -Wconversion -Wwrite-strings -Wstack-protector --std=c99 -D_GNU_SOURCE
DEPFLAGS	+= -MT $@ -MMD -MP -MF $(DEPDIR)/$(notdir $*).Td
LDFLAGS		+= -m64 $(OPTIM)
POSTCOMPILE	= mv -f $(DEPDIR)/$(notdir $*).Td $(DEPDIR)/$(notdir $*).d
GENGETOPTFLAFS += --conf-parser --set-package=$(PROGRAM) --func-name=cmdline_parser_$(PROGRAM) --file-name=$(OBJDIR)/$(CMDLINE) \
				  --set-version="- Copyright (c) $(COPYRIGHT), Michel RIZZO. All Rights Reserved.\n$(PROGRAM) - Version $(VERSION)"
# Gengetopt: to compensate for a build error and an unimplemented feature (?)
POSTGENGOPT	= mv $*.c $*.tmp; \
			  cat $*.tmp | sed \
			  -e "s/my_argv_arg = (char \*\*) malloc((my_argc+1) \* sizeof(char \*));/my_argv_arg = (char **) malloc((unsigned int) (my_argc+1) * sizeof(char *));/" \
			  -e "s/args_info->inputs_num = argc - optind - found_prog_name;/args_info->inputs_num = (unsigned int) (argc - optind - found_prog_name);/" \
			  > $*.c; \
              $(RM) $*.tmp
#-------------------------------------------------------------------------------
all: $(DEPDIR) $(OBJDIR)/$(CMDLINE).c $(OBJDIR)/$(CMDLINE).h $(EXEC)
clean:
	@echo "-- Removing $(EXEC) $(OBJECTS)"
	$(MUTE)$(RM) $(EXEC) $(OBJECTS) $(OBJDIR)/$(CMDLINE).c $(OBJDIR)/$(CMDLINE).h
	$(MUTE)$(RM) $(patsubst %,$(DEPDIR)/%.d,$(basename $(SOURCES))) $(patsubst %,$(DEPDIR)/%.Td,$(basename $(SOURCES)))
	$(MUTE)if [ -d $(DEPDIR) ] && ! ls -A $(DEPDIR)/* > /dev/null 2>&1; then $(RMDIR) $(DEPDIR); fi
	$(MUTE)if [ -d $(OBJDIR) ] && ! ls -A $(OBJDIR)/* > /dev/null 2>&1; then $(RMDIR) $(OBJDIR); fi
astyle:
	@echo "-- Formatting all '.c' and '.h' files"
	$(MUTE)$(ASTYLE) --style=attach --indent=tab --align-pointer=name *.[ch]
cppcheck:
	@echo "-- Static C code analysis"
	$(MUTE)$(CPPCHECK) --std=c99 $(CFLAGS) $(INCLUDES) *.[ch]
#-------------------------------------------------------------------------------
$(EXEC): $(OBJECTS)
	@echo "-- Linking $@"
	$(MUTE)$(CC) $(LDFLAGS) -o $@ $(OBJECTS) $(LIBS)
	$(MUTE)$(STRIP) $@
$(OBJDIR)/%.o: %.c $(DEPDIR)/%.d
	@echo "-- Compiling $@"
	$(MUTE)$(CC) $(DEPFLAGS) $(CCFLAGS) -o $@ -c $(addsuffix .c,$(basename $(notdir $@)))
	$(MUTE)$(POSTCOMPILE)
$(OBJDIR)/$(CMDLINE).o: $(OBJDIR)/$(CMDLINE).c $(OBJDIR)/$(CMDLINE).h $(DEPDIR)/$(CMDLINE).d
	@echo "-- Compiling $@"
	$(MUTE)$(CC) $(DEPFLAGS) $(CCFLAGS) $(OPTIM) -o $@ -c $(addsuffix .c,$(basename $@))
	$(MUTE)$(POSTCOMPILE)
$(OBJDIR)/$(CMDLINE).c $(OBJDIR)/$(CMDLINE).h: $(GGO)
	@echo "-- Generating $*"
	$(MUTE)$(GENGETOPT) $(GENGETOPTFLAFS) < $<
	$(MUTE)$(POSTGENGOPT)
$(DEPDIR)/%.d: ;
$(DEPDIR):
	$(MUTE)$(MKDIR) $(DEPDIR)
#-------------------------------------------------------------------------------
.PRECIOUS: $(DEPDIR)/%.d
.PHONY: all clean astyle cppcheck
#-------------------------------------------------------------------------------
-include $(patsubst %,$(DEPDIR)/%.d,$(basename $(SOURCES)))
#-------------------------------------------------------------------------------
