#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	Internal use.
#------------------------------------------------------------------------------
help:
	@printf "	╔══════════════════════════════════════════════════════════════════════════════╗\n"
	@printf "	║ \e[1mAvailable targets\e[0m                                                            ║\n"
	@printf "	╠══════════════════════════════════════════════════════════════════════════════╣\n"
	@printf "	║ \e[1;33mall\e[0m ................... Build project (debug mode)                           ║\n"
	@printf "	║ \e[1;33mclean\e[0m ................. Clean project                                        ║\n"
	@printf "	║ \e[1;33mastyle\e[0m ................ Format C sources                                     ║\n"
	@printf "	║ \e[1;33mcppcheck\e[0m .............. Static C code analysis                               ║\n"
	@printf "	║ \e[1;33mcommit\e[0m ................ (GIT) Commit and push new version                    ║\n"
	@printf "	╚══════════════════════════════════════════════════════════════════════════════╝\n"
