#------------------------------------------------------------------------------
# Copyright (c) 2020, Michel RIZZO.
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 3
# of the License, or (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#------------------------------------------------------------------------------

#------------------------------------------------------------------------------
# CONTEXT
# -------
#	Top level makefile for C tree structures
#
# INPUTS
# ------
# 	ENVIRONMENT VARIABLES:
#		BIN_DIR			Pathname of released executables
#		LIB_DIR			Pathname of released libraries
#		INC_DIR			Pathname of released header files
#		LANGUAGE		Processing option for language, if any
#
#
# 	MAKE VARIABLES:
#		MUTE			Prevents or not the command line from echoing out to the console
#		DIRS 			List of source directories
#		VERSION 		Version of the jar to generate (come from top level makefile)
#		COPYRIGHT 		Year(s) for copyright clause
#		OPTIM			Optimization options for compilation (and link)
#		STRIP 			Strip utility
#
# NOTES
# -----
#	Independent targets may exist in other parts of parent makefile.
#------------------------------------------------------------------------------
all clean astyle cppcheck::
	$(MUTE)for dir in $(DIRS); do \
		echo "............................................................ $$dir"; \
		cd $$dir; $(MAKE) --no-print-directory $@ MUTE=$(MUTE) VERSION="$(VERSION)" COPYRIGHT="$(COPYRIGHT)" OPTIM="$(OPTIM)" STRIP="$(STRIP)"; cd ..; \
	done
#-------------------------------------------------------------------------------
.PHONY: all clean astyle cppcheck
#-------------------------------------------------------------------------------
