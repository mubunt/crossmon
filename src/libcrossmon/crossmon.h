//------------------------------------------------------------------------------
// Copyright (c) 2021, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: crossmon
// Cross Monitoring, example with two processes
//------------------------------------------------------------------------------
#ifndef CROSSMON_H
#define CROSSMON_H
//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <syslog.h>
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define PROCESSPSEUDOFILESYSTEM		"/proc/self/exe"
#define PROCFS 						"/proc/%d"
#define DEFAULTCONFURATIONFILENAME	"WatchedWatcher.cfg"
#define DEFAULTPERIODICITY 			1		// seconds

#define EXIST(x)					(stat(x, &locstat) < 0 ? 0 : locstat.st_mode)
#define EXISTDIR(y)					(EXIST(y) && (locstat.st_mode & S_IFMT) == S_IFDIR)

#define OPENLOG(ident)				openlog(ident, LOG_CONS | LOG_PID, LOG_USER)
#define LOG(level, format, ...)		syslog(LOG_USER | level, format,  __VA_ARGS__)	// level = LOG_ERR, LOG_WARNING, LOG_NOTICE, LOG_INFO 
#define CLOSELOG()					closelog()

#define SIGNALS(ignore, exit)		struct sigaction act; \
									memset(&act, 0, sizeof(act)); \
									act.sa_sigaction = ignore; \
									act.sa_flags = SA_SIGINFO; \
									sigaction(SIGHUP, &act, NULL); \
									sigaction(SIGINT, &act, NULL); \
									sigaction(SIGQUIT, &act, NULL); \
									sigaction(SIGILL, &act, NULL); \
									sigaction(SIGTRAP, &act, NULL); \
									sigaction(SIGABRT, &act, NULL); \
									sigaction(SIGIOT, &act, NULL); \
									sigaction(SIGBUS, &act, NULL); \
									sigaction(SIGFPE, &act, NULL); \
									sigaction(SIGSEGV, &act, NULL); \
									sigaction(SIGUSR2, &act, NULL); \
									sigaction(SIGPIPE, &act, NULL); \
									sigaction(SIGALRM, &act, NULL); \
									sigaction(SIGTERM, &act, NULL); \
									sigaction(SIGSTKFLT, &act, NULL); \
									sigaction(SIGCONT, &act, NULL); \
									sigaction(SIGTSTP, &act, NULL); \
									sigaction(SIGTTIN, &act, NULL); \
									sigaction(SIGTTOU, &act, NULL); \
									sigaction(SIGURG, &act, NULL); \
									sigaction(SIGXCPU, &act, NULL); \
									sigaction(SIGXFSZ, &act, NULL); \
									sigaction(SIGVTALRM, &act, NULL); \
									sigaction(SIGPROF, &act, NULL); \
									sigaction(SIGWINCH, &act, NULL); \
									sigaction(SIGIO, &act, NULL); \
									sigaction(SIGPOLL, &act, NULL); \
									sigaction(SIGPWR, &act, NULL); \
									sigaction(SIGSYS, &act, NULL); \
									act.sa_sigaction = exit; \
									sigaction(SIGUSR1, &act, NULL)
//------------------------------------------------------------------------------
// EXTERNAL ROUTINES
//------------------------------------------------------------------------------
extern pid_t getPid( char * );
//------------------------------------------------------------------------------
#endif	// CROSSMON_H