//------------------------------------------------------------------------------
// Copyright (c) 2021, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: crossmon
// Cross Monitoring, example with two processes
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <limits.h>
#include <libgen.h>
#include <stdbool.h>
#include <sys/types.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "crossmon.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define FORMAT_PIDOF				"pidof -x %s"
//------------------------------------------------------------------------------
// COMMON FUNCTIONS
//------------------------------------------------------------------------------
pid_t getPid( char *process ) {
	char command[PATH_MAX + 128];
	snprintf(command, sizeof(command) - 1, FORMAT_PIDOF, process);
	FILE *fdcmd = NULL;
	if (NULL == (fdcmd = popen(command, "r"))) return -2;
	char spid[128];
	char *t = fgets(spid, sizeof(spid) - 1, fdcmd);
	pclose(fdcmd);
	if (t == NULL) return -1;
	return (pid_t) strtol(spid, (char **)NULL, 10);
}
//------------------------------------------------------------------------------
