//------------------------------------------------------------------------------
// Copyright (c) 2021, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: crossmon
// Cross Monitoring, example with two processes
//------------------------------------------------------------------------------
#ifndef WATCHED_H
#define WATCHED_H
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define SERVER_BYDEFAULT	"127.0.0.1"
#define CLIENT_PORT			6789
#define MAXSERVERNAME		128
#define MAXCOMMANDSIZE		64
#define SOCKET				int
//--- Common commands to dialog with the server --------------------------------
#define SERVER_DISCONNECT	64
//--- Automaton Commands to dialog with the server -----------------------------
#define VALUES_SEND			1
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
struct __attribute__((aligned(4),packed)) Command_t {
	unsigned int	command;
};
struct __attribute__((aligned(4),packed)) Data_t {
	unsigned int	clientId;
	unsigned int	serialId;
	__uint64_t		time;
	__uint64_t		crc;
};
//------------------------------------------------------------------------------
// EXTERNAL ROUTINES
//------------------------------------------------------------------------------
extern __uint64_t 	_computeCRC( unsigned int, struct Data_t );
extern int			_connectServer( char *, u_int16_t, int * );
extern int			_closeServer( int );
extern int 			_sendCommand2server( int, unsigned int );
extern int			_sendCommand2server2( int, unsigned int, unsigned int, __uint64_t, unsigned int );
//------------------------------------------------------------------------------
#endif	// WATCHED_H