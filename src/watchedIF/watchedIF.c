//------------------------------------------------------------------------------
// Copyright (c) 2021, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: crossmon
// Cross Monitoring, example with two processes
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <limits.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "watchedIF.h"
//------------------------------------------------------------------------------
// GLOBAL FUNCTIONS
//------------------------------------------------------------------------------
__uint64_t _computeCRC( unsigned int command, struct Data_t uplet ) {
	__uint64_t crc = (__uint64_t)command
	                 + (__uint64_t)uplet.clientId
	                 + (__uint64_t)uplet.serialId
	                 + uplet.time;
	return(crc);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _sendCommand2server( int serversock, unsigned int command ) {
	struct Command_t cmd;
	cmd.command = htonl(command);
	if (send(serversock, (void *)&cmd, sizeof(cmd), 0) <= 0) return(-1);
	return(0);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _sendCommand2server2( int serversock, unsigned int command, unsigned int client, __uint64_t time, unsigned int  value ) {
	struct Command_t cmd;
	struct Data_t uplet;
	cmd.command = htonl(command);
	uplet.clientId = htonl(client);
	uplet.serialId = htonl(value);
	uplet.time = htobe64(time);
	uplet.crc  = 0;	// To avoid warning from cppcheck!!!
	uplet.crc = htobe64(_computeCRC(cmd.command, uplet));
	if (0 >= send(serversock, (void *)&cmd, sizeof(cmd), 0)) return(-1);
	if (0 >= send(serversock, (void *)&uplet, sizeof(uplet), 0)) return(-2);
	return(0);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _connectServer( char *srv, uint16_t port, int *serversock ) {
	struct sockaddr_in serv_addr;
	struct hostent *server;
	*serversock = socket(AF_INET, SOCK_STREAM, 0);	// SOCK_NONBLOCK ?
	if (*serversock < 0)  return(-3);
	server = (struct hostent *)gethostbyname(srv);
	if (server == NULL) return(-2);
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, (size_t)server->h_length);
	serv_addr.sin_port = htons(port);
	int n = connect(*serversock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));
	if (n < 0) {
		close(*serversock);
		return(-1);
	}
	return(0);
}
//- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
int _closeServer( int serversock ) {
	int rc = _sendCommand2server(serversock, SERVER_DISCONNECT);
	close(serversock);
	return(rc);
}
//------------------------------------------------------------------------------
