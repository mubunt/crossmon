//------------------------------------------------------------------------------
// Copyright (c) 2021, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: crossmon
// Cross Monitoring, example with two processes
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <limits.h>
#include <libgen.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "watcher_cmdline.h"
#include "crossmon.h"
#include "ini.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define MYIDENTITY		"TheWatcher"
// ...For inih handler
#define MATCH(s, n)		strcmp(section, s) == 0 && strcmp(name, n) == 0
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct {
	// Subset of [Monitoring] section
	char *watched;
	char *watcher;
	char *pngwatcher;
	char *pngpath;
	int periodicity;
} configuration;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static configuration 	config;				// user-definied configuration
static char 			*arguments[4];		// the command line argument
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void myexit( int report ) {
	CLOSELOG();

	if (arguments[0] != NULL) free(arguments[0]);

	if (config.watcher != NULL) free(config.watcher);
	if (config.pngwatcher != NULL) free(config.pngwatcher);
	if (config.watched != NULL) free(config.watched);
	if (config.pngpath != NULL) free(config.pngpath);

	exit(report);
}

static void sighandlerIgnore( int signum, siginfo_t *info, void *ptr ) {
	LOG(LOG_WARNING, "Received signal #%d. Ignoring it.", signum);
}

static void sighandlerExit( int signum, siginfo_t *info, void *ptr ) {
	LOG(LOG_NOTICE, "Received signal #%d. Exit.", signum);
	myexit(EXIT_SUCCESS);
}

static int confighandler(void *user, const char *section, const char *name, const char *value) {
	configuration *pconfig = (configuration *)user;
	if (MATCH("Monitoring", "watched")) {
		if (pconfig->watched != NULL) free(pconfig->watched);
		pconfig->watched = strdup(value);
	} else if (MATCH("Monitoring", "watcher")) {
		if (pconfig->watcher != NULL) free(pconfig->watcher);
		pconfig->watcher = strdup(value);
	} else if (MATCH("Monitoring", "pngwatcher")) {
		if (pconfig->pngwatcher != NULL) free(pconfig->pngwatcher);
		pconfig->pngwatcher = strdup(value);
	} else if (MATCH("Monitoring", "pngpath")) {
		if (pconfig->pngpath != NULL) free(pconfig->pngpath);
		pconfig->pngpath = strdup(value);
	} else if (MATCH("Monitoring", "periodicity")) {
		pconfig->periodicity = (int) strtol(value, (char **)NULL, 10);;
	} else {
		// unknown section/name. Ignore it in this implementation
	}
	return 1;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	//---- Starting logs
	OPENLOG(MYIDENTITY);
	LOG(LOG_NOTICE, "Started %s", MYIDENTITY);
	//---- Get real path of this executable to compute the default path of configuration file
	// /proc/self is a symbolic link to the process-ID subdir
	// of /proc, e.g. /proc/4323 when the pid of the process
	// of this program is 4323.
	// Inside /proc/<pid> there is a symbolic link to the
	// executable that is running as this <pid>.  This symbolic
	// link is called "exe".
	// So if we read the path where the symlink /proc/self/exe
	// points to we have the full path of the executable.
	char currentname[PATH_MAX];
	if (readlink(PROCESSPSEUDOFILESYSTEM, currentname, PATH_MAX - 1) == -1) {
		LOG(LOG_ERR, "%s", "Cannot get path of the current executable. Exit.");
		myexit(EXIT_FAILURE);
	}
	char *currentdirname, *currentbasename, *dirc, *basec;
	dirc = strdup(currentname);
	basec = strdup(currentname);
	currentdirname = dirname(dirc);
	currentbasename = basename(basec);
	char configurationFile[PATH_MAX];
	snprintf(configurationFile, sizeof(configurationFile) - 1, "%s/%s", currentdirname, DEFAULTCONFURATIONFILENAME);
	//---- Parameter checking and setting
	struct gengetopt_args_info	args_info;
	if (cmdline_parser_watcher(argc, argv, &args_info) != 0) {
		LOG(LOG_ERR, "%s", "Cannot get command-line parameter(s). Exit.");
		myexit(EXIT_FAILURE);
	}
	//---- Get configuration file
	if (args_info.config_given) {
		arguments[1] = argv[1];
		arguments[2] = argv[2];
		arguments[3] = NULL;
		strncpy(configurationFile, args_info.config_arg, sizeof(configurationFile) - 1);
	} else {
		arguments[1] = NULL;
	}
	//---- Clean allocations done for parameters
	cmdline_parser_watcher_free(&args_info);
	//---- Configuration checking and setting
	LOG(LOG_INFO, "Configuration file is %s", configurationFile);
	struct stat locstat;
	if (! EXIST(configurationFile)) {
		LOG(LOG_ERR, "Non-existent configuration file '%s'. Exit.", configurationFile);
		myexit(EXIT_FAILURE);
	}
	config.watched = config.watcher = config.pngwatcher = NULL;
	config.periodicity = DEFAULTPERIODICITY;
	if (ini_parse(configurationFile, confighandler, &config) < 0) {
		LOG(LOG_ERR, "Cannot load configuration file '%s'. Exit.", configurationFile);
		myexit(EXIT_FAILURE);
	}
	if (config.watched == NULL) {
		LOG(LOG_ERR, "%s. Exit.", "Process to monitor not specified");
		myexit(EXIT_FAILURE);
	}
	if (config.watcher == NULL) {
		LOG(LOG_ERR, "%s. Exit.", "Monitoring process not specified");
		myexit(EXIT_FAILURE);
	}
	if (strcmp(config.watcher, currentbasename) != 0) {
		LOG(LOG_ERR, "Current process is not the specified monitoring process ('%s' vs. '%s'). Exit.", config.watcher, currentbasename);
		myexit(EXIT_FAILURE);
	}
	//--- process to monitor (watched)
	char processToWatch[PATH_MAX];
	snprintf(processToWatch, sizeof(processToWatch) - 1, "%s/%s", currentdirname, config.watched);
	arguments[0] = malloc(strlen(processToWatch) + 1);
	strcpy(arguments[0], processToWatch);
	LOG(LOG_INFO, "Process to watch is %s", processToWatch);
	//---- Signals
	SIGNALS(sighandlerIgnore, sighandlerExit);
	//----  Go on
	while (1) {
		pid_t pidToWatch = getPid(processToWatch);
		if (pidToWatch == -2) {
			LOG(LOG_ERR, "Cannot get pid of process '%s' ('pidof' fails). Exit", processToWatch);
			myexit(EXIT_FAILURE);
		}
		if (pidToWatch == -1) {
			LOG(LOG_INFO, "Process %s is not running. Launching it", processToWatch);
			while(1) {
				if ((pidToWatch = fork()) < 0) {			// fork a child process
					LOG(LOG_ERR, "Forking child process failed: %s. Exit.", strerror(errno));
					myexit(EXIT_FAILURE);
				}
				if (pidToWatch == 0) {						// for the child process
					if (execvp(*arguments, arguments) < 0) {
						LOG(LOG_ERR, "Execing process '%s' failed: %s. Exit.", processToWatch, strerror(errno));
						myexit(EXIT_FAILURE);
					}
				}
				int status;
				pid_t w = waitpid(pidToWatch, &status, WUNTRACED | WCONTINUED);
				if (w == -1) {
					LOG(LOG_ERR, "Cannot wait pid of process '%s': %s. Exit.", processToWatch, strerror(errno));
					myexit(EXIT_FAILURE);
				}
				if (WIFEXITED(status)) {
					LOG(LOG_INFO, "Process '%s' exited, status=%d. Launching it again.", processToWatch, WEXITSTATUS(status));
				} else if (WIFSIGNALED(status)) {
					LOG(LOG_INFO, "Process '%s' killed by signal %d. Launching it again.", processToWatch, WTERMSIG(status));
				} else if (WIFSTOPPED(status)) {
					LOG(LOG_INFO, "Process '%s' stopped by signal %d. Launching it again.", processToWatch, WSTOPSIG(status));
				}
			}
		}
		sleep(DEFAULTPERIODICITY);
	}
	//---- Never reached......
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
