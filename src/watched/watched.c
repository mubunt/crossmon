//------------------------------------------------------------------------------
// Copyright (c) 2021, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: crossmon
// Cross Monitoring, example with two processes
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>
#include <errno.h>
#include <semaphore.h>
#include <pthread.h>
#include <stdbool.h>
#include <ctype.h>
#include <libgen.h>
#include <errno.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <sys/param.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <netinet/in.h>
#include <arpa/inet.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "watched_cmdline.h"
#include "crossmon.h"
#include "ini.h"
#include "watchedIF.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define MYIDENTITY		"TheWatched"
// ...For inih handler
#define MATCH(s, n)		strcmp(section, s) == 0 && strcmp(name, n) == 0
#define BACKLOG			10	// how many pending connections queue will hold
//------------------------------------------------------------------------------
// STRUCTURES
//------------------------------------------------------------------------------
typedef struct {
	// Subset of [Monitoring] section
	char *watched;
	char *pngwatched;
	char *watcher;
	char *pngpath;
	int periodicity;
	// [Server] section
	char *logfile;
} configuration;
//------------------------------------------------------------------------------
// LOCAL VARIABLES
//------------------------------------------------------------------------------
static configuration 	config;				// user-definied configuration
static char 			*arguments[4];		// the command line argument
static FILE 			*fdlog;
static SOCKET 			serversock = (SOCKET) -1;
static SOCKET 			clientsock = (SOCKET) -1;
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void getTimestamp( char *ts ) {
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	sprintf(ts, "%02d:%02d:%02d", timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
}

static void mylog(const char *format, ...) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	fprintf(fdlog, "%s\n", buff);
	fflush(fdlog);
	va_end(argp);
}

static void myexit( int report ) {
	// Check if there is something in socked before closint it
	if (clientsock != -1) {
		struct timeval tv;
		fd_set fdread;
		tv.tv_sec = 1;
		tv.tv_usec = 0;
		FD_ZERO(&fdread);
		FD_SET( clientsock, &fdread );
		if (select(clientsock + 1, &fdread, NULL, NULL, &tv) > 0) {
			LOG(LOG_WARNING, "%s", "!!! Socket not empty !!!");
		}
		close(clientsock);
	}
	if (serversock != -1) close(serversock);

	if (fdlog != NULL) {
		char timestamp[16];
		getTimestamp(timestamp);
		mylog("== EXIT ON %s =====\n", timestamp);
		fclose(fdlog);
	}

	CLOSELOG();

	if (arguments[0] != NULL) free(arguments[0]);

	if (config.watcher != NULL) free(config.watcher);
	if (config.watched != NULL) free(config.watched);
	if (config.pngwatched != NULL) free(config.pngwatched);
	if (config.pngpath != NULL) free(config.pngpath);
	if (config.logfile != NULL) free(config.logfile);

	exit(report);
}

static void sighandlerIgnore( int signum, siginfo_t *info, void *ptr ) {
	LOG(LOG_WARNING, "Received signal #%d. Ignoring it.", signum);
}

static void sighandlerExit( int signum, siginfo_t *info, void *ptr ) {
	LOG(LOG_NOTICE, "Received signal #%d. Exit.", signum);
	myexit(EXIT_SUCCESS);
}

static int confighandler(void *user, const char *section, const char *name, const char *value) {
	configuration *pconfig = (configuration *)user;
	if (MATCH("Monitoring", "watched")) {
		if (pconfig->watched != NULL) free(pconfig->watched);
		pconfig->watched = strdup(value);
	} else if (MATCH("Monitoring", "pngwatched")) {
		if (pconfig->pngwatched != NULL) free(pconfig->pngwatched);
		pconfig->pngwatched = strdup(value);
	} else if (MATCH("Monitoring", "watcher")) {
		if (pconfig->watcher != NULL) free(pconfig->watcher);
		pconfig->watcher = strdup(value);
	} else if (MATCH("Monitoring", "pngpath")) {
		if (pconfig->pngpath != NULL) free(pconfig->pngpath);
		pconfig->pngpath = strdup(value);
	} else if (MATCH("Server", "log")) {
		if (pconfig->logfile != NULL) free(pconfig->logfile);
		pconfig->logfile = strdup(value);
	} else if (MATCH("Monitoring", "periodicity")) {
		pconfig->periodicity = (int) strtol(value, (char **)NULL, 10);;
	} else {
		// unknown section/name. Ignore it in this implementation
	}
	return 1;
}

static char *convert(__uint64_t timer) {
	struct tm *t;
	time_t tim;
	char *pt;

	tim = (time_t) timer;
	t = localtime(&tim);
	pt = asctime(t);
	if (pt[strlen(pt) - 1] == '\n') pt[strlen(pt) - 1] = '\0';
	return(pt);
}

static bool testCRC(unsigned int command, struct Data_t uplet) {
	if (uplet.crc != _computeCRC(command, uplet)) return false;
	return true;
}

static bool setConnection(const char *serverip, uint16_t port, SOCKET *server, struct sockaddr_in *serv_addr) {
	*server = socket(AF_INET, SOCK_STREAM, 0);	// SOCK_NONBLOCK ?
	if (*server < 0) {
		LOG(LOG_ERR, "[%d] Opening socket failed: %s", port, strerror(errno));
		return false;
	}
// LOG(LOG_INFO, "[%d] Socket opened", port);
	// try to avoid the pesky "Address already in use" error message
	int reuse = 1;
	if (setsockopt(*server, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &reuse, sizeof(int)) == -1) {
		LOG(LOG_ERR, "[%d] Setsockopt failed: %s", port, strerror(errno));
		close(*server);
		return false;
	}
	/*
		// connection will close immediately after closing the program
		struct linger lin;
		lin.l_onoff = 1;
		lin.l_linger = 0;
		if (setsockopt(*server, SOL_SOCKET, SO_LINGER, (const char *)&lin, sizeof(lin)) == -1) {
			LOG(LOG_ERR, "[%d] Setsockopt failed [SO_LINGER]: %s", port, strerror(errno));
			close(*server);
			return false;
		}
	*/
	(void) bzero((char *) serv_addr, sizeof(*serv_addr));
	serv_addr->sin_family = AF_INET;
	serv_addr->sin_addr.s_addr = inet_addr(serverip);
	//serv_addr->sin_addr.s_addr = INADDR_ANY;
	serv_addr->sin_port = htons(port);
	if (bind(*server, (struct sockaddr *) serv_addr, sizeof(*serv_addr)) < 0) {
		LOG(LOG_ERR, "[%d] Binding failed:%s", port, strerror(errno));
		close(*server);
		return false;
	}
// LOG(LOG_INFO,  "[%d] Binding socket done", port);
	if (listen(*server, BACKLOG) < 0 ) {
		LOG(LOG_ERR, "[%d] listen failed:%s", port, strerror(errno));
		close(*server);
		return false;
	}
	LOG(LOG_INFO, "[%d] Server is up and running", port);
	return true;
}
//------------------------------------------------------------------------------
// ACQUISUITION THREAD
//------------------------------------------------------------------------------
static void *dataAcquisitionThread( void *arg  __attribute__((__unused__)) ) {
	struct sockaddr_in serv_addr;

	LOG(LOG_INFO, "%s", "Starting acquisition thread");
	while (1) {
		if (setConnection(SERVER_BYDEFAULT, CLIENT_PORT, &serversock, &serv_addr)) break;
		usleep(100000);
	}
	size_t length = sizeof(serv_addr);
	while (1) {
		while (1) {
			clientsock = accept(serversock, (struct sockaddr *) &serv_addr, (socklen_t *) &length);
			if (clientsock >= 0) break;
			LOG(LOG_ERR, "[%d] Cannot accept connexion to socket: %s", CLIENT_PORT, strerror(errno));
		}
		struct Command_t cmd, cmd2;
		struct Data_t clientuplet;
		bool endofloop = false;
		while (1) {
			ssize_t rc;
			if (0 > (rc = recv(clientsock, (void *)&cmd2, sizeof(cmd2), 0))) {
				LOG(LOG_ERR, "[%d] Receiving command failed: %s", CLIENT_PORT, strerror(errno));
				break;
			}
			if (rc == 0) {
				cmd.command = SERVER_DISCONNECT;
			} else {
				cmd.command = ntohl(cmd2.command);
			}
			switch (cmd.command) {
			case VALUES_SEND:
				if (0 > recv(clientsock, (void *)&clientuplet, sizeof(clientuplet), 0)) {
					LOG(LOG_ERR, "[port %d] Receiving data failed: %s", CLIENT_PORT, strerror(errno));
				} else {
					clientuplet.crc = be64toh(clientuplet.crc);
					if (testCRC(cmd2.command, clientuplet)) {
						clientuplet.clientId = ntohl(clientuplet.clientId);
						clientuplet.serialId = ntohl(clientuplet.serialId);
						clientuplet.time = be64toh(clientuplet.time);
						mylog("Client %03d - Id %05d - %-22s", clientuplet.clientId, clientuplet.serialId, convert(clientuplet.time));
					} else {
						LOG(LOG_ERR, "Received inconsistent data (wrong CRC) [port %d]", CLIENT_PORT);
					}
				}
				break;
			case SERVER_DISCONNECT:
				endofloop = true;
				break;
			default:
				LOG(LOG_ERR, "[%d] Received UNKNOWN command", CLIENT_PORT);
				break;
			}
			if (endofloop) break;
		}
		close(clientsock);
	}
	LOG(LOG_INFO, "%s", "Stopping acquisition thread");
	pthread_exit(0);
	return 0;
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	//---- Starting logs
	OPENLOG(MYIDENTITY);
	LOG(LOG_NOTICE, "Started %s", MYIDENTITY);
	//---- Get real path of this executable to compute the default path of configuration file
	// /proc/self is a symbolic link to the process-ID subdir
	// of /proc, e.g. /proc/4323 when the pid of the process
	// of this program is 4323.
	// Inside /proc/<pid> there is a symbolic link to the
	// executable that is running as this <pid>.  This symbolic
	// link is called "exe".
	// So if we read the path where the symlink /proc/self/exe
	// points to we have the full path of the executable.
	char currentname[PATH_MAX];
	if (readlink(PROCESSPSEUDOFILESYSTEM, currentname, PATH_MAX - 1) == -1) {
		LOG(LOG_ERR, "%s", "Cannot get path of the current executable. Exit.");
		myexit(EXIT_FAILURE);
	}
	char *currentdirname, *currentbasename, *dirc, *basec;
	dirc = strdup(currentname);
	basec = strdup(currentname);
	currentdirname = dirname(dirc);
	currentbasename = basename(basec);
	char configurationFile[PATH_MAX];
	snprintf(configurationFile, sizeof(configurationFile) - 1, "%s/%s", currentdirname, DEFAULTCONFURATIONFILENAME);
	//---- Parameter checking and setting
	struct gengetopt_args_info	args_info;
	if (cmdline_parser_watched(argc, argv, &args_info) != 0) {
		LOG(LOG_ERR, "%s", "Cannot get command-line parameter(s). Exit.");
		myexit(EXIT_FAILURE);
	}
	//---- Get configuration file
	if (args_info.config_given) {
		arguments[1] = argv[1];
		arguments[2] = argv[2];
		arguments[3] = NULL;
		strncpy(configurationFile, args_info.config_arg, sizeof(configurationFile) - 1);
	} else {
		arguments[1] = NULL;
	}
	//---- Clean allocations done for parameters
	cmdline_parser_watched_free(&args_info);
	//---- Configuration checking and setting
	LOG(LOG_INFO, "Configuration file is %s", configurationFile);
	struct stat locstat;
	if (! EXIST(configurationFile)) {
		LOG(LOG_ERR, "Non-existent configuration file '%s'. Exit.", configurationFile);
		myexit(EXIT_FAILURE);
	}
	config.watched = config.watcher = config.pngwatched = NULL;
	config.periodicity = DEFAULTPERIODICITY;
	if (ini_parse(configurationFile, confighandler, &config) < 0) {
		LOG(LOG_ERR, "Cannot load configuration file '%s'. Exit.", configurationFile);
		myexit(EXIT_FAILURE);
	}
	if (config.watcher == NULL) {
		LOG(LOG_ERR, "%s. Exit.", "Monitoring process not specified");
		myexit(EXIT_FAILURE);
	}
	if (config.watched == NULL) {
		LOG(LOG_ERR, "%s. Exit.", "Process to monitor not specified");
		myexit(EXIT_FAILURE);
	}
	if (strcmp(config.watched, currentbasename) != 0) {
		LOG(LOG_ERR, "Current process is not the specified monitored process ('%s' vs. '%s'). Exit.", config.watched, currentbasename);
		myexit(EXIT_FAILURE);
	}
	if (config.logfile == NULL) {
		LOG(LOG_ERR, "%s. Exit.", "No log file specified");
		myexit(EXIT_FAILURE);
	}
	//--- Log file
	fdlog = fopen(config.logfile, "a");
	if (fdlog == NULL) {
		LOG(LOG_ERR, "Cannot create/open log file %s (%s). Exit.", config.logfile, strerror(errno));
		myexit(EXIT_FAILURE);
	}
	char timestamp[16];
	getTimestamp(timestamp);
	mylog("== LOG ON %s ======\n", timestamp);
	//--- process to monitor (watcher)
	char processWatcher[PATH_MAX];
	snprintf(processWatcher, sizeof(processWatcher) - 1, "%s/%s", currentdirname, config.watcher);
	arguments[0] = malloc(strlen(processWatcher) + 1);
	strcpy(arguments[0], processWatcher);
	LOG(LOG_INFO, "Watcher process is %s", processWatcher);
	//---- Signals
	SIGNALS(sighandlerIgnore, sighandlerExit);
	//----  Go on
	// Creation of 1 thread to communicate one with several data acquisition clients.
	pthread_t th_acquisition;
	if (pthread_create(&th_acquisition, NULL, dataAcquisitionThread, NULL) < 0) {
		LOG(LOG_ERR, "%s. Exit.", "Cannot create data acquisition thread");
		myexit(EXIT_FAILURE);
	}
	LOG(LOG_INFO, "%s", "Acquisition thread successfully created");
	// Monitoring loop
	while (1) {
		pid_t pidWatcher = getPid(processWatcher);
		if (pidWatcher == -2) {
			LOG(LOG_ERR, "Cannot get pid of process '%s' ('pidof' fails). Exit", processWatcher);
			myexit(EXIT_FAILURE);
		}
		if (pidWatcher == -1) {
			LOG(LOG_INFO, "Process %s is not running. Launching it", processWatcher);
			while(1) {
				if ((pidWatcher = fork()) < 0) {			// fork a child process
					LOG(LOG_ERR, "Forking child process failed: %s. Exit.", strerror(errno));
					myexit(EXIT_FAILURE);
				}
				if (pidWatcher == 0) {						// for the child process
					if (clientsock != -1) close(clientsock);
					if (serversock != -1) close(serversock);
					clientsock = serversock = (SOCKET) -1;
					if (execvp(*arguments, arguments) < 0) {
						LOG(LOG_ERR, "Execing process '%s' failed: %s. Exit.", processWatcher, strerror(errno));
						myexit(EXIT_FAILURE);
					}
				}
				int status;
				pid_t w = waitpid(pidWatcher, &status, WUNTRACED | WCONTINUED);
				if (w == -1) {
					LOG(LOG_ERR, "Cannot wait pid of process '%s': %s. Exit.", processWatcher, strerror(errno));
					myexit(EXIT_FAILURE);
				}
				if (WIFEXITED(status)) {
					LOG(LOG_INFO, "Process '%s' exited, status=%d. Launching it again.", processWatcher, WEXITSTATUS(status));
				} else if (WIFSIGNALED(status)) {
					LOG(LOG_INFO, "Process '%s' killed by signal %d. Launching it again.", processWatcher, WTERMSIG(status));
				} else if (WIFSTOPPED(status)) {
					LOG(LOG_INFO, "Process '%s' stopped by signal %d. Launching it again.", processWatcher, WSTOPSIG(status));
				}
			}
		}
		sleep(DEFAULTPERIODICITY);
	}
	//---- Never reached......
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
