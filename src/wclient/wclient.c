//------------------------------------------------------------------------------
// Copyright (c) 2021, Michel RIZZO.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Project: crossmon
// Client for Cross Monitoring, example with two processes
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// SYSTEM HEADER FILES
//------------------------------------------------------------------------------
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
//------------------------------------------------------------------------------
// APPLICATION HEADER FILES
//------------------------------------------------------------------------------
#include "wclient_cmdline.h"
#include "watchedIF.h"
//------------------------------------------------------------------------------
// MACROS
//------------------------------------------------------------------------------
#define WARNING_PREFIX		"WARNING: "
#define FATALERROR_PREFIX	"FATAL ERROR: "
#define ESC_BOLDRED			"\033[1;31m"
#define ESC_BOLDYELLOW		"\033[1;33m"
#define ESC_BOLDCYAN		"\033[1;36m"
#define ESC_STOP			"\033[0m"
//------------------------------------------------------------------------------
// LOCAL FUNCTIONS
//------------------------------------------------------------------------------
static void _fatalError(const char *format, ...) {
	va_list al;
	char buff[512];
	strcpy(buff, "\n");
	strcat(buff, FATALERROR_PREFIX);
	va_start(al, format);
	vsprintf(buff + strlen(FATALERROR_PREFIX) + 1, format, al);
	strcat(buff, ".\n\n");
	va_end(al);
	fprintf(stderr, ESC_BOLDRED "%s" ESC_STOP, buff);
	exit(EXIT_FAILURE);
}
static void _warning(const char *format, ...) {
	va_list al;
	char buff[512];
	strcpy(buff, WARNING_PREFIX);
	va_start(al, format);
	vsprintf(buff + strlen(WARNING_PREFIX), format, al);
	strcat(buff, ".\n");
	va_end(al);
	fprintf(stderr, ESC_BOLDYELLOW "%s" ESC_STOP, buff);
	fflush(stderr);
}
static void _log(const char *format, ...) {
	va_list argp;
	char buff[512];
	va_start(argp, format);
	vsprintf(buff, format, argp);
	fprintf(stdout, ESC_BOLDCYAN "%s" ESC_STOP, buff);
	fflush(stdout);
	va_end(argp);
}
static void _signalHandler( int sig __attribute__((__unused__)) ) {
	_fatalError("Interrupt catched. Stopping ..");
}
static void _threadSleep(int32_t iTimeOut) {
	struct timeval t;

	if (iTimeOut >= 1000) {
		t.tv_sec = iTimeOut / 1000;
		t.tv_usec = (iTimeOut % 1000) * 1000;
	} else {
		t.tv_sec = 0;
		t.tv_usec = (iTimeOut % 1000) * 1000;
	}
	select( 0, NULL, NULL, NULL, &t);
}
//------------------------------------------------------------------------------
// MAIN
//------------------------------------------------------------------------------
int main(int argc, char **argv) {
	//---- Parameter checking and setting --------------------------------------
	struct gengetopt_args_info args_info;
	if (cmdline_parser_wclient(argc, argv, &args_info) != 0)
		exit(EXIT_FAILURE);
	unsigned int clientID = (unsigned int)args_info.ident_arg;
	cmdline_parser_wclient_free(&args_info);
	//----  Go on --------------------------------------------------------------
	signal(SIGABRT, &_signalHandler);
	signal(SIGTERM, &_signalHandler);
	signal(SIGINT, &_signalHandler);

	char serverID[MAXSERVERNAME];
	strcpy(serverID, SERVER_BYDEFAULT);

	uint16_t portID;
	portID = CLIENT_PORT;

	unsigned int serial = 0;
	int socketServer;
	while (1) {
		while (1) {
			int n;
			if (0 == (n = _connectServer(serverID, portID, &socketServer))) break;
			_warning("Cannot connect socket (%d) with Server (%s): %d. Wait 5 second", portID, serverID, n);
			_threadSleep(5000);
		}
		__uint64_t timer = (__uint64_t)time(NULL);
		_log("...Client %03d: Sending record (%lld, %05d)\n", clientID, timer, serial);
		int n = _sendCommand2server2(socketServer, VALUES_SEND, clientID, timer, serial);
		if (n != 0)
			_warning("Cannot send command (%d): %d. Retry.", socketServer, n);
		int p = _closeServer(socketServer);
		if (p != 0)
			_fatalError("Cannot disconnect to server (%s): %d. Exit", serverID, p);
		if (n == 0) {
			_threadSleep(1000);	// 1 second
			++serial;
		}
	}
	//---- Exit ----------------------------------------------------------------
	// Never reached
	return EXIT_SUCCESS;
}
//------------------------------------------------------------------------------
