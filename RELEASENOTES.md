# RELEASE NOTES: *crossmon*, Cross Monitoring, example with two processes.

Functional limitations, if any, of this version are described in the *README.md* file.

**Version 0.1.0**:
  - First version. Monitoring mecchanism seems to work fine. *watcher* and *watched* are correctly relaunched when they are stopped using signal SIGUSR1 or SIGKILL. However, we have an issue when *watched* (the server) is a child of the top process (PID #1) and is stopped; when it is relaunched, the communication with clients is frozen after some exchanges with some losts of messages  (connect <--> accept issue).... **STILL UNDER DEBUGGING**

**Version 1.0.0**:
  - Some sources "enhancements".
  - Fixed the above issue: forgot to close socket descriptors of parent process in child (after fork)!
  - Do not forget: use the command *netstat -tulpe* to knowd the status of our sockets...
